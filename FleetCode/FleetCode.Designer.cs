﻿namespace FleetCode {
    partial class FleetCode {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FleetCode));
            this.fleet1 = new System.Windows.Forms.CheckBox();
            this.fleet2 = new System.Windows.Forms.CheckBox();
            this.fleet3 = new System.Windows.Forms.CheckBox();
            this.fleet4 = new System.Windows.Forms.CheckBox();
            this.btn_generate = new System.Windows.Forms.Button();
            this.btn_copy = new System.Windows.Forms.Button();
            this.outputBox = new System.Windows.Forms.TextBox();
            this.autoCopy = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // fleet1
            // 
            this.fleet1.AutoSize = true;
            this.fleet1.Location = new System.Drawing.Point(10, 9);
            this.fleet1.Name = "fleet1";
            this.fleet1.Size = new System.Drawing.Size(74, 17);
            this.fleet1.TabIndex = 0;
            this.fleet1.Text = "第一舰队";
            this.fleet1.UseVisualStyleBackColor = true;
            // 
            // fleet2
            // 
            this.fleet2.AutoSize = true;
            this.fleet2.Location = new System.Drawing.Point(90, 9);
            this.fleet2.Name = "fleet2";
            this.fleet2.Size = new System.Drawing.Size(74, 17);
            this.fleet2.TabIndex = 1;
            this.fleet2.Text = "第二舰队";
            this.fleet2.UseVisualStyleBackColor = true;
            // 
            // fleet3
            // 
            this.fleet3.AutoSize = true;
            this.fleet3.Location = new System.Drawing.Point(170, 9);
            this.fleet3.Name = "fleet3";
            this.fleet3.Size = new System.Drawing.Size(74, 17);
            this.fleet3.TabIndex = 2;
            this.fleet3.Text = "第三舰队";
            this.fleet3.UseVisualStyleBackColor = true;
            // 
            // fleet4
            // 
            this.fleet4.AutoSize = true;
            this.fleet4.Location = new System.Drawing.Point(250, 9);
            this.fleet4.Name = "fleet4";
            this.fleet4.Size = new System.Drawing.Size(74, 17);
            this.fleet4.TabIndex = 3;
            this.fleet4.Text = "第四舰队";
            this.fleet4.UseVisualStyleBackColor = true;
            // 
            // btn_generate
            // 
            this.btn_generate.Location = new System.Drawing.Point(138, 32);
            this.btn_generate.Name = "btn_generate";
            this.btn_generate.Size = new System.Drawing.Size(81, 23);
            this.btn_generate.TabIndex = 8;
            this.btn_generate.Text = "导出代码";
            this.btn_generate.UseVisualStyleBackColor = true;
            this.btn_generate.Click += new System.EventHandler(this.btn_generate_Click);
            // 
            // btn_copy
            // 
            this.btn_copy.Location = new System.Drawing.Point(225, 32);
            this.btn_copy.Name = "btn_copy";
            this.btn_copy.Size = new System.Drawing.Size(99, 23);
            this.btn_copy.TabIndex = 9;
            this.btn_copy.Text = "复制到剪切板";
            this.btn_copy.UseVisualStyleBackColor = true;
            this.btn_copy.Click += new System.EventHandler(this.btn_copy_Click);
            // 
            // outputBox
            // 
            this.outputBox.Location = new System.Drawing.Point(10, 59);
            this.outputBox.Name = "outputBox";
            this.outputBox.ReadOnly = true;
            this.outputBox.Size = new System.Drawing.Size(314, 20);
            this.outputBox.TabIndex = 10;
            // 
            // autoCopy
            // 
            this.autoCopy.AutoSize = true;
            this.autoCopy.Checked = true;
            this.autoCopy.CheckState = System.Windows.Forms.CheckState.Checked;
            this.autoCopy.Location = new System.Drawing.Point(10, 36);
            this.autoCopy.Name = "autoCopy";
            this.autoCopy.Size = new System.Drawing.Size(122, 17);
            this.autoCopy.TabIndex = 11;
            this.autoCopy.Text = "自动复制到剪切板";
            this.autoCopy.UseVisualStyleBackColor = true;
            // 
            // FleetCode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(336, 90);
            this.Controls.Add(this.autoCopy);
            this.Controls.Add(this.outputBox);
            this.Controls.Add(this.btn_copy);
            this.Controls.Add(this.btn_generate);
            this.Controls.Add(this.fleet4);
            this.Controls.Add(this.fleet3);
            this.Controls.Add(this.fleet2);
            this.Controls.Add(this.fleet1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FleetCode";
            this.Text = "导出舰队配置代码";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox fleet1;
        private System.Windows.Forms.CheckBox fleet2;
        private System.Windows.Forms.CheckBox fleet3;
        private System.Windows.Forms.CheckBox fleet4;
        private System.Windows.Forms.Button btn_generate;
        private System.Windows.Forms.Button btn_copy;
        private System.Windows.Forms.TextBox outputBox;
        private System.Windows.Forms.CheckBox autoCopy;
    }
}

