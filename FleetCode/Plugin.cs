﻿using ElectronicObserver.Window.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ElectronicObserver.Window;

namespace FleetCode {

    public class Plugin : ServerPlugin {

        public override string MenuTitle {
            get {
                return "舰队配置代码提取器(&F)";
            }
        }

        //public override Form GetToolWindow() {
        //    return new FleetCode();
        //}

        FormMain main;

        public override bool RunService(FormMain main) {
            this.main = main;
            for (int i = 0; i < main.fFleet.Length; i++) {
                // Fleet ID starts with 1
                AddContextMenu(main.fFleet[i], i + 1);
            }

            return true;
        }

        void AddContextMenu(FormFleet fleet, int index) {
            var contextField = typeof(FormFleet).GetField("ContextMenuFleet", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            if (contextField != null) {
                var menu = (ContextMenuStrip)contextField.GetValue(fleet);
                var item = new ToolStripMenuItem("导出当前舰队配置代码(&F)");
                item.Tag = index;
                item.Click += getCurrentFleetCode;
                menu.Items.Add(item);
                item = new ToolStripMenuItem("导出舰队配置代码");
                item.Tag = index;
                item.Click += openFleetCodeWindow;
                menu.Items.Add(item);
                item = new ToolStripMenuItem("导出装备代码(&E)");
                item.Tag = index;
                item.Click += getEquipments;
                menu.Items.Add(item);
            }
        }

        private void openFleetCodeWindow(object sender, EventArgs e) {
            new FleetCode((int)((ToolStripMenuItem)sender).Tag).Show(main);
        }

        private void getCurrentFleetCode(object sender, EventArgs e) {
            FleetCode fc = new FleetCode((int)((ToolStripMenuItem)sender).Tag);
            Clipboard.SetText(fc.getFleetCode());
        }

        private void getEquipments(object sender, EventArgs e) {
            EquipmentCode ec = new EquipmentCode();
            Clipboard.SetText(ec.getEquipmentCode());
        }

    }

}
