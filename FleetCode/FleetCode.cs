﻿using ElectronicObserver.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FleetCode {
    public partial class FleetCode : Form {

        private CheckBox[] fleetSelected;

        /// <summary>
        /// 0..3: to be filled by 4 fleetTemplate strings, which represent 4 fleets
        /// </summary>
        readonly private String outputTemplate = "[{0},{1},{2},{3}]";

        /// <summary>
        /// 0: ShipData.ShipID
        /// 1: ShipData.Level
        /// 2: ShipData.LuckTotal
        /// 3: equipments e.g. [eID1,eID2] [eID1,eID2,eID3,eID4]
        /// 4: equipment modification/aircraft pilot skill levels e.g. [0,0] [0,0,0,0]
        /// 5: repeat #4 for compatibility with "WhoCallsTheFleet"
        /// </summary>
        readonly private String shipTemplate = "[{0},[{1},{2}],{3},{4},{5}]";

        readonly private String nullFleet = "[]";

        public FleetCode(int x) {
            InitializeComponent();
            // null at Index 0 for padding because fleet ID starts with 1
            fleetSelected = new CheckBox[] { null, fleet1, fleet2, fleet3, fleet4 };
            fleetSelected[x].Checked = true;
        }

        private void btn_copy_Click(object sender, EventArgs e) {
            if (outputBox.Text != null)
                Clipboard.SetText(outputBox.Text);
        }

        private void btn_generate_Click(object sender, EventArgs e) {
            outputBox.Text = getFleetCode();
            if (autoCopy.Checked)
                Clipboard.SetText(outputBox.Text);
        }

        internal String getFleetCode() {
            String[] fleets = fillFleets();
            String result = String.Format(outputTemplate, fleets);
            return result;
        }

        private String[] fillFleets() {
            String[] result = new String[4];
            for (int i = 0; i < 4; i++)
                // fleet ID starts from 1, not 0
                result[i] = fillFleet(i + 1);
            return result;
        }

        private String fillFleet(int x) {
            if (!fleetSelected[x].Checked)
                return nullFleet;
            FleetData fleet = KCDatabase.Instance.Fleet[x];
            if (fleet == null || fleet.MembersInstance.Sum(s => s != null ? s.Level : 0) == 0)
                return nullFleet;
            StringBuilder sb = new StringBuilder().Append("[");
            for (int i = 0; i < 6; i++) {
                int shipID = KCDatabase.Instance.Fleet[x][i];
                if (shipID == -1)
                    break;
                sb.Append(fillShip(KCDatabase.Instance.Ships[shipID])).Append(",");
            }
            sb.Length--;
            sb.Append("]");
            return sb.ToString();
        }

        private String fillShip(ShipData ship) {
            String[] shipParam = new String[6];
            shipParam[0] = ship.ShipID.ToString();
            shipParam[1] = ship.Level.ToString();
            shipParam[2] = ship.LuckTotal.ToString();
            shipParam[3] = fillEquipments(ship);
            shipParam[4] = fillEquipmentLevel(ship);
            shipParam[5] = shipParam[4];
            String result = String.Format(shipTemplate, shipParam);
            return result;
        }

        private String fillEquipments(ShipData ship) {
            // if ship has no equip on
            if (ship.SlotInstance[0] == null)
                return "[]";
            StringBuilder sb = new StringBuilder();
            sb.Append("[").Append(ship.SlotInstance[0].EquipmentID);
            for (int i = 1; i < 4; i++)
                if (ship.SlotInstance[i] == null)
                    break;
                else
                    sb.Append(",").Append(ship.SlotInstance[i].EquipmentID);
            sb.Append("]");
            return sb.ToString();
        }

        private String fillEquipmentLevel(ShipData ship) {
            // if ship has no equip on
            if (ship.SlotInstance[0] == null)
                return "[]";
            StringBuilder sb = new StringBuilder();
            sb.Append("[").Append(Math.Max(ship.SlotInstance[0].Level, ship.SlotInstance[0].AircraftLevel));
            for (int i = 1; i < 4; i++)
                if (ship.SlotInstance[i] == null)
                    break;
                else
                    sb.Append(",").Append(Math.Max(ship.SlotInstance[i].Level, ship.SlotInstance[i].AircraftLevel));
            sb.Append("]");
            return sb.ToString();
        }

    }
}
