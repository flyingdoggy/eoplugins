﻿using ElectronicObserver.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FleetCode {
    class EquipmentCode {
        public EquipmentCode() {
        }

        internal String getEquipmentCode() {
            if (KCDatabase.Instance.Equipments.Count == 0)
                return "[[],[]]";
            // index starts from 1 and ends with count number
            String result = "[{0},{1}]";
            StringBuilder eqid = new StringBuilder().Append("[");
            StringBuilder eqlv = new StringBuilder().Append("[");
            // index starts from 1 and ends with count number
            foreach (KeyValuePair<int, EquipmentData> eq in KCDatabase.Instance.Equipments) {
                eqid.Append(eq.Value.EquipmentID).Append(",");
                eqlv.Append(Math.Max(eq.Value.Level, eq.Value.AircraftLevel)).Append(",");
            }
            eqid.Length--;
            eqlv.Length--;
            eqid.Append("]");
            eqlv.Append("]");
            result = String.Format(result, eqid.ToString(), eqlv.ToString());
            return result;
        }
    }
}
