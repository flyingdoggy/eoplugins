﻿using ElectronicObserver.Data;
using ElectronicObserver.Utility.Mathematics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElectronicObserver.Resource.Record {

	[DebuggerDisplay( "{Record.Count} Records" )]
	public class ExpeditionRecord : RecordBase {

		[DebuggerDisplay( "[{ExpeditionID}] : {ExpeditionName}" )]
		public class ExpeditionElement : RecordElementBase {

			/// <summary>
			/// 远征名称
			/// </summary>
			public string ExpeditionName { get; set; }

			/// <summary>
			/// 远征日期
			/// </summary>
			public DateTime Date { get; set; }

			/// <summary>
			/// 结果
			/// </summary>
			public string Result { get; set; }

			/// <summary>
			/// 提督经验
			/// </summary>
			public int HQExp { get; set; }

			/// <summary>
			/// 舰娘经验
			/// </summary>
			public int ShipExp { get; set; }

			/// <summary>
			/// 获取资源
			/// </summary>
			public int[] Material { get; set; }

			/// <summary>
			/// 道具1名称
			/// </summary>
			public string Item1Name { get; set; }

			/// <summary>
			/// 道具1数量
			/// </summary>
			public int Item1Count { get; set; }

			/// <summary>
			/// 道具2名称
			/// </summary>
			public string Item2Name { get; set; }

			/// <summary>
			/// 道具2数量
			/// </summary>
			public int Item2Count { get; set; }


			public ExpeditionElement() {
				Date = DateTime.Now;
			}

			public ExpeditionElement( string line )
				: base( line ) { }


			public override void LoadLine( string line ) {

				string[] elem = line.Split( ",".ToCharArray() );
				if ( elem.Length < 13 ) throw new ArgumentException( "元素个数不正确。" );

				ExpeditionName = elem[0];
				Date = DateTimeHelper.CSVStringToTime( elem[1] );
				Result = elem[2];
				HQExp = int.Parse( elem[3] );
				ShipExp = int.Parse( elem[4] );
				Material = new int[]
				{
					int.Parse( elem[5] ),
					int.Parse( elem[6] ),
					int.Parse( elem[7] ),
					int.Parse( elem[8] )
				};
				Item1Name = elem[9];
				Item1Count = string.IsNullOrEmpty( elem[10] ) ? 0 : int.Parse( elem[10] );
				Item2Name = elem[11];
				Item2Count = string.IsNullOrEmpty( elem[12] ) ? 0 : int.Parse( elem[12] );

			}

			public override string SaveLine() {

				return string.Format( "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}",
					ExpeditionName,
					DateTimeHelper.TimeToCSVString( Date ),
					Result,
					HQExp,
					ShipExp,
					string.Join( ",", Material ),
					Item1Name,
					Item1Count == 0 ? "" : Item1Count.ToString(),
					Item2Name,
					Item2Count == 0 ? "" : Item2Count.ToString() );

			}
		}



		public List<ExpeditionElement> Record { get; private set; }


		public ExpeditionRecord()
			: base() {
			Record = new List<ExpeditionElement>();
		}

		public ExpeditionElement this[int i] {
			get { return Record[i]; }
			set { Record[i] = value; }
		}


		protected override void LoadLine( string line ) {
			Record.Add( new ExpeditionElement( line ) );
		}

		protected override string SaveLines() {

			StringBuilder sb = new StringBuilder();

			var list = new List<ExpeditionElement>( Record );
			list.Sort( ( e1, e2 ) => e1.Date.CompareTo( e2.Date ) );

			foreach ( var elem in list ) {
				sb.AppendLine( elem.SaveLine() );
			}

			return sb.ToString();
		}


		protected override void ClearRecord() {
			Record.Clear();
		}


		protected override bool IsAppend { get { return true; } }


		public override bool Load( string path ) {
			return true;
		}

		public override bool Save( string path ) {
			bool ret = base.Save( path );

			Record.Clear();
			return ret;
		}



		protected override string RecordHeader {
			get { return "远征名,时间,结果,提督经验,舰娘经验,油,弹,钢,航空铝,道具1,个数,道具2,个数"; }
		}

		public override string FileName {
			get { return "ExpeditionRecord.csv"; }
		}
	}
}
