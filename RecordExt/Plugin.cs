﻿using ElectronicObserver.Window.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ElectronicObserver.Window;
using ElectronicObserver.Resource.Record;
using ElectronicObserver.Observer;
using ElectronicObserver.Data;

namespace RecordExt
{
	public class Plugin : ServerPlugin
	{
		ExpeditionRecord expeditionRecord = new ExpeditionRecord();

		public override string MenuTitle
		{
			get
			{
				return "记录扩展";
			}
		}

		public override bool RunService( FormMain main )
		{
			expeditionRecord.Load( RecordManager.Instance.MasterPath );

			main.FormClosing += Main_FormClosing;

			APIObserver.Instance.APIList["api_req_mission/result"].ResponseReceived += Updated;

			return true;
		}

		private void Updated( string apiname, dynamic data )
		{

			var item = new ExpeditionRecord.ExpeditionElement();
			item.ExpeditionName = data.api_quest_name;
			item.Result = Constants.GetExpeditionResult( (int)data.api_clear_result );
			item.HQExp = (int)data.api_get_exp;
			item.ShipExp = ( (int[])data.api_get_ship_exp ).Min();
			try
			{
				item.Material = (int[])data.api_get_material;
			}
			catch
			{
				item.Material = new int[4];
			}
			if ( data.api_get_item1() )//&& data.api_get_item1.api_useitem_id >= 0 )
			{
				item.Item1Name = data.api_get_item1.api_useitem_name;
				item.Item1Count = (int)data.api_get_item1.api_useitem_count;
			}
			if ( data.api_get_item2() )//&& data.api_get_item2.api_useitem_id >= 0 )
			{
				item.Item2Name = data.api_get_item2.api_useitem_name;
				item.Item2Count = (int)data.api_get_item2.api_useitem_count;
			}

			expeditionRecord.Record.Add( item );

		}

		private void Main_FormClosing( object sender, System.Windows.Forms.FormClosingEventArgs e )
		{
			expeditionRecord.Save( RecordManager.Instance.MasterPath );
		}
	}
}
