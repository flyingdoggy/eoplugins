﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AFT {

    class Slot {

        /// <summary>
        /// Images used to paint the canvas.
        /// </summary>
        public static ImageResources resources {
            private get;
            set;
        }

        /// <summary>
        /// Column of this slot in the resultTable.
        /// Seems redundant :/
        /// </summary>
        public int column {
            get;
            private set;
        }

        /// <summary>
        /// Row of this slot in the resultTable.
        /// </summary>
        public int row {
            get;
            private set;
        }

        /// <summary>
        /// State of the slot.
        /// </summary>
        public SlotState state {
            get;
            private set;
        }

        /// <summary>
        /// Aircraft capability of the slot.
        /// </summary>
        public int space {
            get;
            set;
        }

        /// <summary>
        /// Equipment ID in the slot.
        /// </summary>
        public int equipID {
            get;
            set;
        }

        /// <summary>
        /// Type of the equipment in the slot. This should be equal to EquipmentType[2].
        /// </summary>
        public int equipType {
            get;
            set;
        }

        /// <summary>
        /// Button control used to display image as well as changing states, etc.
        /// </summary>
        public Button canvas {
            get;
            private set;
        }

        /// <summary>
        /// Indicates if the slot can carry aircraft that provides air superiority.
        /// </summary>
        public SlotType carrierFlag {
            get;
            private set;
        }

        /// <summary>
        /// True if assets archive is available, otherwise false.
        /// </summary>
        private bool assests {
            get;
            set;
        }

        /// <summary>
        /// Construct a new Slot object with given row and column.
        /// </summary>
        /// <param name="r">Row</param>
        /// <param name="c">Column</param>
        /// <param name="aFlag">Assets archive availability</param>
        public Slot(int r, int c, bool aFlag) {
            canvas = new Button();
            canvas.Margin = new Padding(0);
            canvas.Dock = DockStyle.Fill;
            canvas.BackgroundImageLayout = ImageLayout.Center;
            canvas.Click += new EventHandler(this.canvasClick);
            canvas.Paint += new PaintEventHandler(this.paintCanvas);

            // flatten the button to make it look like a canvas
            canvas.FlatStyle = FlatStyle.Flat;
            // remove the boarder
            canvas.FlatAppearance.BorderSize = 0;
            // make it unfocusable
            canvas.TabStop = false;

            assests = aFlag;
            column = c;
            row = r;
            state = SlotState.Disabled;
            carrierFlag = SlotType.NA;
            equipID = -1;
            equipType = -1;
        }

        private void paintCanvas(object sender, PaintEventArgs e) {
            if (state == SlotState.Disabled || equipID == -1) {
                return;
            }

            if (assests == true) {
                Image bg = resources.Equipment[equipID.ToString()];
                Image icon = resources.Icon[equipType.ToString()];
                e.Graphics.DrawImageUnscaled(bg, 0, -20);
                int x = (40 - icon.Width) / 2;
                e.Graphics.DrawImage(icon, x, x);
            } else {
                // TODO text mode draw
            }
        }

        private void canvasClick(object sender, EventArgs e) {
            // give focus to the parent control which is the resultTable
            // so the button would not look bad when the AFT window loses focus
            canvas.Parent.Focus();
            switch (state) {
                case SlotState.Disabled:
                    return;
                case SlotState.Aircraft:
                    state = SlotState.MCrew;
                    equipID = 108; // 熟練艦載機整備員
                    equipType = 35;
                    break;
                case SlotState.MCrew:
                    state = SlotState.NotUsed;
                    equipID = 42; // 応急修理要員
                    equipType = 23;
                    break;
                case SlotState.NotUsed:
                    switch (carrierFlag) {
                        case SlotType.CV:
                            equipID = 22; // Reppuu 烈風
                            equipType = 6;
                            break;
                        case SlotType.BBV:
                            equipID = 26; // Zuiun 瑞雲
                            equipType = 11;
                            break;
                    }
                    state = SlotState.Aircraft;
                    break;
                case SlotState.Bismarck:
                    AFT aft = (AFT)canvas.Parent.Parent;
                    aft.Bismarck(row);
                    break;
            }
            rePaint();
        }

        public void Bismarck() {
            equipID = 43; // 応急修理女神
            equipType = 23;
            rePaint();
        }

        public void rePaint() {
            canvas.Invalidate();
        }

        /// <summary>
        /// Reset the canvas of the slot. This will reset equipment information and repaint canvas.
        /// </summary>
        /// <param name="bFlag">If slot belongs to a Bismarck, set to true; otherwise false.</param>
        /// <param name="cFlag">Indicates what kind of aircraft does the slot carry.</param>
        /// <param name="space">Aircraft capacity of the slot.</param>
        public void resetCanvas(bool bFlag, SlotType cFlag, int space) {
            equipID = -1;
            equipType = -1;
            carrierFlag = cFlag;
            // disable canvas if slot cannot provide air superiority
            switch (carrierFlag) {
                case SlotType.NA:
                    state = SlotState.Disabled;
                    break;
                case SlotType.BBV:
                    state = SlotState.Aircraft;
                    equipID = 26; // Zuiun 瑞雲
                    equipType = 11;
                    break;
                case SlotType.CV:
                    state = SlotState.Aircraft;
                    equipID = 22; // Reppuu 烈風
                    equipType = 6;
                    break;
            }
            if (bFlag == true) {
                state = SlotState.Bismarck;
            }
            this.space = space;
            rePaint();
        }

    }

    enum SlotState {
        /// <summary>
        /// Initial slot state, canvas displays nothing. Also used for unavailable slots.
        /// </summary>
        Disabled,
        /// <summary>
        /// Slot will be equipped with aircrafts excluding carrier-based recon plane.
        /// </summary>
        Aircraft,
        /// <summary>
        /// Slot will be equipped with experienced aircraft maintenance crew.
        /// </summary>
        MCrew,
        /// <summary>
        /// Slot is available but does not participate calculation. This is different from Disabled state.
        /// Slot in such state should draw damage control crew in canvas while Disabled state draws nothing.
        /// </summary>
        NotUsed,
        /// <summary>
        /// Slot belongs to a Bismarck. Canvas draws damage control goddess when clicked.
        /// </summary>
        Bismarck
    };

    enum SlotType {
        /// <summary>
        /// Slot does not provide air superiority.
        /// </summary>
        NA,
        /// <summary>
        /// Slot can be equipped with carrier-based aircrafts.
        /// </summary>
        CV,
        /// <summary>
        /// Slot can be equipped with seaplanes.
        /// </summary>
        BBV
    }

}
