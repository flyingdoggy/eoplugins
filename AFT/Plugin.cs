﻿using ElectronicObserver.Window.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AFT {

    public class Plugin : DialogPlugin {

        public override string MenuTitle {
            get {
                return "对空配置计算器(&A)";
            }
        }

        public override Form GetToolWindow() {
            return new AFT();
        }

    }

}
