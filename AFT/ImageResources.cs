﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElectronicObserver.Resource;

namespace AFT {
    class ImageResources {

        public ImageCollection Portrait {
            get;
            private set;
        }
        public ImageCollection Icon {
            get;
            private set;
        }
        public ImageCollection Equipment {
            get;
            private set;
        }

        public ImageResources() {
            Portrait = new ImageCollection();
            Icon = new ImageCollection();
            Equipment = new ImageCollection();
        }

    }
}
