﻿namespace AFT {
    partial class AFT {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AFT));
            this.calTypeRegularMap = new System.Windows.Forms.RadioButton();
            this.calTypePlayerCustom = new System.Windows.Forms.RadioButton();
            this.calTypePlayerDefine = new System.Windows.Forms.RadioButton();
            this.regularMap1 = new System.Windows.Forms.ComboBox();
            this.regularMap2 = new System.Windows.Forms.ComboBox();
            this.playerCustom = new System.Windows.Forms.ComboBox();
            this.playerDefine = new System.Windows.Forms.TextBox();
            this.btn_calculate = new System.Windows.Forms.Button();
            this.DEBUGBOX = new System.Windows.Forms.TextBox();
            this.btn_getFleet = new System.Windows.Forms.Button();
            this.resultTable = new System.Windows.Forms.TableLayoutPanel();
            this.SuspendLayout();
            // 
            // calTypeRegularMap
            // 
            this.calTypeRegularMap.AutoSize = true;
            this.calTypeRegularMap.Checked = true;
            this.calTypeRegularMap.Location = new System.Drawing.Point(12, 12);
            this.calTypeRegularMap.Name = "calTypeRegularMap";
            this.calTypeRegularMap.Size = new System.Drawing.Size(73, 17);
            this.calTypeRegularMap.TabIndex = 0;
            this.calTypeRegularMap.TabStop = true;
            this.calTypeRegularMap.Text = "常规地图";
            this.calTypeRegularMap.UseVisualStyleBackColor = true;
            this.calTypeRegularMap.CheckedChanged += new System.EventHandler(this.changeCalType);
            // 
            // calTypePlayerCustom
            // 
            this.calTypePlayerCustom.AutoSize = true;
            this.calTypePlayerCustom.Location = new System.Drawing.Point(12, 35);
            this.calTypePlayerCustom.Name = "calTypePlayerCustom";
            this.calTypePlayerCustom.Size = new System.Drawing.Size(78, 17);
            this.calTypePlayerCustom.TabIndex = 1;
            this.calTypePlayerCustom.TabStop = true;
            this.calTypePlayerCustom.Text = "预设/自订";
            this.calTypePlayerCustom.UseVisualStyleBackColor = true;
            this.calTypePlayerCustom.CheckedChanged += new System.EventHandler(this.changeCalType);
            // 
            // calTypePlayerDefine
            // 
            this.calTypePlayerDefine.AutoSize = true;
            this.calTypePlayerDefine.Location = new System.Drawing.Point(12, 58);
            this.calTypePlayerDefine.Name = "calTypePlayerDefine";
            this.calTypePlayerDefine.Size = new System.Drawing.Size(73, 17);
            this.calTypePlayerDefine.TabIndex = 2;
            this.calTypePlayerDefine.TabStop = true;
            this.calTypePlayerDefine.Text = "指定目标";
            this.calTypePlayerDefine.UseVisualStyleBackColor = true;
            this.calTypePlayerDefine.CheckedChanged += new System.EventHandler(this.changeCalType);
            // 
            // regularMap1
            // 
            this.regularMap1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.regularMap1.FormattingEnabled = true;
            this.regularMap1.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6"});
            this.regularMap1.Location = new System.Drawing.Point(91, 8);
            this.regularMap1.Name = "regularMap1";
            this.regularMap1.Size = new System.Drawing.Size(46, 21);
            this.regularMap1.TabIndex = 3;
            // 
            // regularMap2
            // 
            this.regularMap2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.regularMap2.FormattingEnabled = true;
            this.regularMap2.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6"});
            this.regularMap2.Location = new System.Drawing.Point(143, 8);
            this.regularMap2.Name = "regularMap2";
            this.regularMap2.Size = new System.Drawing.Size(46, 21);
            this.regularMap2.TabIndex = 4;
            // 
            // playerCustom
            // 
            this.playerCustom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.playerCustom.FormattingEnabled = true;
            this.playerCustom.Items.AddRange(new object[] {
            "开发中"});
            this.playerCustom.Location = new System.Drawing.Point(91, 34);
            this.playerCustom.Name = "playerCustom";
            this.playerCustom.Size = new System.Drawing.Size(98, 21);
            this.playerCustom.TabIndex = 5;
            // 
            // playerDefine
            // 
            this.playerDefine.Location = new System.Drawing.Point(91, 58);
            this.playerDefine.Name = "playerDefine";
            this.playerDefine.Size = new System.Drawing.Size(98, 20);
            this.playerDefine.TabIndex = 6;
            this.playerDefine.Text = "999";
            // 
            // btn_calculate
            // 
            this.btn_calculate.Location = new System.Drawing.Point(483, 8);
            this.btn_calculate.Name = "btn_calculate";
            this.btn_calculate.Size = new System.Drawing.Size(107, 70);
            this.btn_calculate.TabIndex = 7;
            this.btn_calculate.Text = "计算";
            this.btn_calculate.UseVisualStyleBackColor = true;
            this.btn_calculate.Click += new System.EventHandler(this.doCalculation);
            // 
            // DEBUGBOX
            // 
            this.DEBUGBOX.Location = new System.Drawing.Point(12, 365);
            this.DEBUGBOX.Multiline = true;
            this.DEBUGBOX.Name = "DEBUGBOX";
            this.DEBUGBOX.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DEBUGBOX.Size = new System.Drawing.Size(577, 362);
            this.DEBUGBOX.TabIndex = 9;
            // 
            // btn_getFleet
            // 
            this.btn_getFleet.Location = new System.Drawing.Point(370, 8);
            this.btn_getFleet.Name = "btn_getFleet";
            this.btn_getFleet.Size = new System.Drawing.Size(107, 70);
            this.btn_getFleet.TabIndex = 10;
            this.btn_getFleet.Text = "获取配置\r\n(第一舰队)";
            this.btn_getFleet.UseVisualStyleBackColor = true;
            this.btn_getFleet.Click += new System.EventHandler(this.getFleetSetup);
            // 
            // resultTable
            // 
            this.resultTable.AutoSize = true;
            this.resultTable.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.resultTable.ColumnCount = 5;
            this.resultTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.resultTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.resultTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.resultTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.resultTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 102F));
            this.resultTable.Location = new System.Drawing.Point(12, 82);
            this.resultTable.Name = "resultTable";
            this.resultTable.RowCount = 6;
            this.resultTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.resultTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.resultTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.resultTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.resultTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.resultTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.resultTable.Size = new System.Drawing.Size(568, 247);
            this.resultTable.TabIndex = 12;
            // 
            // AFT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(607, 739);
            this.Controls.Add(this.resultTable);
            this.Controls.Add(this.btn_getFleet);
            this.Controls.Add(this.DEBUGBOX);
            this.Controls.Add(this.btn_calculate);
            this.Controls.Add(this.playerDefine);
            this.Controls.Add(this.playerCustom);
            this.Controls.Add(this.regularMap2);
            this.Controls.Add(this.regularMap1);
            this.Controls.Add(this.calTypePlayerDefine);
            this.Controls.Add(this.calTypePlayerCustom);
            this.Controls.Add(this.calTypeRegularMap);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AFT";
            this.Text = "对空配置计算器";
            this.Load += new System.EventHandler(this.AFT_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton calTypeRegularMap;
        private System.Windows.Forms.RadioButton calTypePlayerCustom;
        private System.Windows.Forms.RadioButton calTypePlayerDefine;
        private System.Windows.Forms.ComboBox regularMap1;
        private System.Windows.Forms.ComboBox regularMap2;
        private System.Windows.Forms.ComboBox playerCustom;
        private System.Windows.Forms.TextBox playerDefine;
        private System.Windows.Forms.Button btn_calculate;
        private System.Windows.Forms.TextBox DEBUGBOX;
        private System.Windows.Forms.Button btn_getFleet;
        private System.Windows.Forms.TableLayoutPanel resultTable;
    }
}