﻿using ElectronicObserver.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Compression;
using System.IO;

namespace AFT {

    public partial class AFT : Form {

        private CalculationType calType;
        private bool calculator_ready;
        private Control[] shipIMG;
        private Slot[] slots;
        private ShipData[] ships;
        private List<EquipmentData> aircrafts;
        private const int _SHIPIMGW = 160;
        private const int _SHIPIMGH = 40;
        private const int _MAXSHIP = 6;
        private const int _MAXSLOT = 4;
        private bool assets;
        private ImageResources resources;
        private const int _MAXSHIPID = 500;
        private const int _MAXEQUIPID = 141;

        #region loadAssets

        private bool loadAssets() {
            try {
                using (ZipArchive archive = ZipFile.OpenRead("AFTAssets.zip")) {
                    // load Equipment images
                    string bst = "AFTAssets/Equipment/";
                    for (int i = 1; i <= _MAXEQUIPID; i++) {
                        try {
                            ZipArchiveEntry entry = archive.GetEntry(bst + i + ".png");
                            resources.Equipment.Images.Add(i.ToString(), Image.FromStream(entry.Open(), true));
                        } catch (Exception) {
                            continue;
                        }
                    }

                    // load Portrait imagese
                    bst = "AFTAssets/Portrait/";
                    for (int i = 1; i <= _MAXSHIPID; i++) {
                        try {
                            ZipArchiveEntry entry = archive.GetEntry(bst + i + ".png");
                            resources.Portrait.Images.Add(i.ToString(), Image.FromStream(entry.Open(), true));
                        } catch (Exception) {
                            continue;
                        }
                    }

                    // load Icon images
                    bst = "AFTAssets/Icon/";
                    addIconToResources(archive, bst + "Aircraft_6.png", "6");
                    addIconToResources(archive, bst + "Aircraft_7.png", "7");
                    addIconToResources(archive, bst + "Aircraft_8.png", "8");
                    addIconToResources(archive, bst + "Aircraft_11.png", "11");
                    addIconToResources(archive, bst + "DamageControl.png", "23");
                    addIconToResources(archive, bst + "MCrew.png", "35");
                }
                return true;
            } catch (Exception ex) {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        private void addIconToResources(ZipArchive archive, string path, string key) {
            ZipArchiveEntry entry = archive.GetEntry(path);
            resources.Icon.Images.Add(key, Image.FromStream(entry.Open(), true));
        }

        #endregion

        public AFT() {
            InitializeComponent();
            calType = CalculationType.RegularMap;
            calculator_ready = false;
            resources = new ImageResources();

            // init panel array for displaying ship portraits later
            shipIMG = new Panel[_MAXSHIP];
            for (int i = 0; i < _MAXSHIP; i++) {
                shipIMG[i] = new Panel();
                shipIMG[i].BackgroundImageLayout = ImageLayout.Center;
                shipIMG[i].Margin = new System.Windows.Forms.Padding(0);
                shipIMG[i].Padding = new System.Windows.Forms.Padding(0);
                shipIMG[i].Width = _SHIPIMGW;
                shipIMG[i].Height = _SHIPIMGH;
                resultTable.Controls.Add(shipIMG[i], 0, i);
            }

            // load assets package
            assets = loadAssets();
            if (assets == false) {
                DEBUGBOX.Text = "图像资源载入失败(AFTAssets.zip)";
            } else {
                Slot.resources = this.resources;
            }

            slots = new Slot[_MAXSLOT * _MAXSHIP];
            for (int row = 0; row < _MAXSHIP; row++)
                for (int column = 0; column < _MAXSLOT; column++) {
                    int k = row * _MAXSLOT + column;
                    slots[k] = new Slot(row, column + 1, assets);
                    resultTable.Controls.Add(slots[k].canvas, column + 1, row);
                }
        }

        // event handler for calType radio buttons; change calculation type 
        private void changeCalType(object sender, EventArgs e) {
            RadioButton btn = (RadioButton)sender;
            if (btn.Checked == true) {
                switch (btn.Name) {
                    case "calTypeRegularMap":
                        calType = CalculationType.RegularMap;
                        break;
                    case "calTypePlayerCustom":
                        calType = CalculationType.PlayerCustom;
                        break;
                    case "calTypePlayerDefine":
                        calType = CalculationType.PlayerDefine;
                        break;
                }
            }
        }

        // copy-pasted from Calculator.cs(Calculator_Load)
        private void AFT_Load(object sender, EventArgs e) {
            if (Owner != null) {
                this.Location = Point.Add(Owner.Location, new Size(40, 40));
            }

            this.AutoScaleMode = AutoScaleMode.None;
            // the following statement causes scaling issues without the above one/or change order
            this.Font = ElectronicObserver.Utility.Configuration.Config.UI.MainFont;
        }

        private void doCalculation(object sender, EventArgs e) {
            // return if fleet and equipment info is not ready
            if (calculator_ready == false)
                return;

            int target = 0;
            // TODO get target AS value for the other two calculation types
            switch (calType) {
                case CalculationType.RegularMap:
                    target = 0;
                    break;
                case CalculationType.PlayerCustom:
                    target = 0;
                    break;
                case CalculationType.PlayerDefine:
                    try {
                        target = Convert.ToInt32(playerDefine.Text);
                        if (target <= 0)
                            throw new FormatException();
                    } catch (Exception) {
                        MessageBox.Show("然而目标制空值并非正整数 :(");
                        playerDefine.Text = "999";
                        return;
                    }
                    break;
            }

            // TODO knapsack problem algorithm
        }

        private void resetResultTable() {
            for (int i = 0; i < _MAXSHIP; i++) {
                ShipData ship = ships[i];
                if (ship != null) {
                    ShipDataMaster shipM = ship.MasterShip;
                    shipIMG[i].BackgroundImage = resources.Portrait[ship.ShipID.ToString()];
                    int j;
                    bool bFlag;
                    if (new[] { 171, 172, 173, 178 }.Contains(ship.ShipID))
                        bFlag = true;
                    else
                        bFlag = false;
                    SlotType cflag;
                    switch (shipM.ShipType) {
                        // CVL
                        case 7:
                        // CV
                        case 11:
                        // Akitsu Maru - Amphibious Assault Ship
                        case 17:
                        // Taihou - Armored Carrier
                        case 18:
                            cflag = SlotType.CV;
                            break;
                        // CAV
                        case 6:
                        // BBV
                        case 10:
                        // SSV
                        case 14:
                        // AV - Seaplane Tender
                        case 16:
                            cflag = SlotType.BBV;
                            break;
                        default:
                            cflag = SlotType.NA;
                            break;
                    }
                    for (j = 0; j < shipM.SlotSize; j++) {
                        slots[i * _MAXSLOT + j].resetCanvas(bFlag, cflag, shipM.Aircraft[j]);
                    }
                    for (; j < _MAXSLOT; j++) {
                        slots[i * _MAXSLOT + j].resetCanvas(bFlag, SlotType.NA, 0);
                    }
                } else {
                    shipIMG[i].BackgroundImage = null;
                }
            }
        }

        private void getFleetSetup(object sender, EventArgs e) {
            calculator_ready = false;
            if (KCDatabase.Instance == null)
                return;

            // retrieve ships in Fleet 1
            int[] shipid = new int[_MAXSHIP];
            ships = new ShipData[_MAXSHIP];
            for (int i = 0; i < _MAXSHIP; i++) {
                shipid[i] = KCDatabase.Instance.Fleet[1][i];
                ShipData ship = KCDatabase.Instance.Ships[shipid[i]];
                ships[i] = ship;
            }

            aircrafts = new List<EquipmentData>();

            //*
            // retrieve all aircrafts in depo
            int all = KCDatabase.Instance.Equipments.Count;
            int count = 0;
            // index starts from 1 and ends with count number
            foreach (KeyValuePair<int, EquipmentData> eq in KCDatabase.Instance.Equipments) {
                switch (eq.Value.MasterEquipment.EquipmentType[2]) {
                    // fighter
                    case 6:
                    // dive bomber
                    case 7:
                    // torpedo bomber
                    case 8:
                    // recon
                    case 9:
                    // seaplane
                    default:
                        count++;
                        //sb.Append("KeyValuePair Key=" + eq.Key + " Value=" + eq.Value.NameWithLevel + " EquipmentID=" + eq.Value.EquipmentID + "\r\n");
                        //sb.Append("EquipmentTypeArray:");
                        //for (int j = 0; j < eq.Value.MasterEquipment.EquipmentType.Count; j++) {
                        //    sb.Append(" " + eq.Value.MasterEquipment.EquipmentType[j]);
                        //}
                        //sb.Append("\r\n");
                        break;
                }
                // MasterID is the player's unique equipment ID
                // EquipmentID is the equipment's ID(same equipments have same EquipmentID)
            }//*/


            // reset resultTable and canvas, get ready for calculation
            resetResultTable();
            // necessary data acquired, set calculator_ready flag to true
            calculator_ready = true;
        }

        public void Bismarck(int row) {
            for (int i = 0; i < _MAXSLOT; i++)
                slots[row * _MAXSLOT + i].Bismarck();
        }

        #region CodeNoLongerUsed

        /*
        private void SCRAPPEDgetFleetSetup(object sender, EventArgs e) {
            // retrieve KC ship id and player ship id and slot capacity and equipments on shipid
            for (int i = 0; i < _MAXSHIP; i++) {
                ShipData ship = KCDatabase.Instance.Ships[shipid[i]];
                if (ship != null) {
                    // MasterID is the player's unique ship ID
                    // ShipID is the ship's ID(same shipid have same ShipID)
                    sb.Append("Ship " + (i + 1) + "(" + ship.Name + ")" + " InternalShipID=" + ship.ShipID + " PlayerShipID=" + ship.MasterID + "\r\n");
                    sb.Append("Slot Count: " + ship.MasterShip.SlotSize + "@" + ship.MasterShip.Aircraft[0]);
                    for (int j = 1; j < ship.MasterShip.SlotSize; j++)
                        sb.Append("/" + ship.MasterShip.Aircraft[j]);
                    //sb.Append("\r\nEquipped Items: ");
                    sb.Append("\r\nslots array: ");
                    int[] slots = new int[ship.Slot.Count];
                    slots = ship.Slot.ToArray();
                    for (int j = 0; j < slots.Length; j++)
                        sb.Append(slots[j] + " ");
                    sb.Append("\r\n");
                    for (int j = 0; j < slots.Length - 1; j++) {
                        EquipmentData eq = KCDatabase.Instance.Equipments[slots[j]];
                        if (eq != null) {
                            sb.Append(eq.Name + " ");
                        } else {
                            break;
                        }
                    }
                    sb.Append("\r\n");
                } else {
                    break;
                }
            }
        }//*/

        #endregion


    }

    enum CalculationType {
        RegularMap,
        PlayerCustom,
        PlayerDefine
    };
}
