﻿using ElectronicObserver.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExpCalculator
{
	public partial class Calculator : Form
	{
		/// <summary>
		/// Completely experience table from 1 to 150. Each line = 20 levels
		/// </summary>
		public static readonly int[] ExpTable = new int[]
		{
			0, 0, 100, 300, 600, 1000, 1500, 2100, 2800, 3600, 4500, 5500, 6600, 7800, 9100, 10500, 12000, 13600, 15300, 17100, 19000,
			21000, 23100, 25300, 27600, 30000, 32500, 35100, 37800, 40600, 43500, 46500, 49600, 52800, 56100, 59500, 63000, 66600, 70300, 74100, 78000,
			82000, 86100, 90300, 94600, 99000, 103500, 108100, 112800, 117600, 122500, 127500, 132700, 138100, 143700, 149500, 155500, 161700, 168100, 174700, 181500,
			188500, 195800, 203400, 211300, 219500, 228000, 236800, 245900, 255300, 265000, 275000, 285400, 296200, 307400, 319000, 331000, 343400, 356200, 369400, 383000,
			397000, 411500, 426500, 442000, 458000, 474500, 491500, 509000, 527000, 545500, 564500, 584500, 606500, 631500, 661500, 701500, 761500, 851500, 1000000, 1000000,
			1010000, 1011000, 1013000, 1016000, 1020000, 1025000, 1031000, 1038000, 1046000, 1055000, 1065000, 1077000, 1091000, 1107000, 1125000, 1145000, 1168000, 1194000, 1223000, 1255000,
			1290000, 1329000, 1372000, 1419000, 1470000, 1525000, 1584000, 1647000, 1714000, 1785000, 1860000, 1940000, 2025000, 2115000, 2210000, 2310000, 2415000, 2525000, 2640000, 2760000,
			2887000, 3021000, 3162000, 3310000, 3465000, 3628000, 3799000, 3978000, 4165000, 4360000
		};

		/// <summary>
		/// Sea exp table.
		/// </summary>
		public static readonly Dictionary<string, int> SeaExpTable = new Dictionary<string, int>
		{
			{"1-1",  30}, {"1-2",  50}, {"1-3",  80}, {"1-4", 100}, {"1-5", 150},
			{"2-1", 120}, {"2-2", 150}, {"2-3", 200}, {"2-4", 300}, {"2-5", 250},
			{"3-1", 310}, {"3-2", 320}, {"3-3", 330}, {"3-4", 350}, {"3-5", 400},
			{"4-1", 310}, {"4-2", 320}, {"4-3", 330}, {"4-4", 340},
			{"5-1", 360}, {"5-2", 380}, {"5-3", 400}, {"5-4", 420}, {"5-5", 450},
			{"6-1", 400}, {"6-2", 420},
		};

		public static readonly decimal[] ResultMulties = new[] { 1.2M, 1M, 1M, 0.8M, 0.7M, 0.5M };

		private ShipItem selectedShip;

		public Calculator()
		{
			InitializeComponent();

			int startExp = int.Parse( textStartExp.Text );
			int endExp = int.Parse( textBoxEndExp.Text );

			bool start = false;
			bool end = false;
			for ( int i = ExpTable.Length - 1; i >= 0; i-- )
			{
				if ( start && end )
					break;

				if ( !start && ExpTable[i] <= startExp )
				{
					numericStartLevel.Value = i;
					start = true;
				}
				if ( !end && ExpTable[i] <= endExp )
				{
					numericEndLevel.Value = i;
					end = true;
				}
			}
		}

		private void Calculator_Load( object sender, EventArgs e )
		{
			if ( Owner != null )
			{
				this.Location = Point.Add( Owner.Location, new Size( 40, 40 ) );
			}

			this.Font = ElectronicObserver.Utility.Configuration.Config.UI.MainFont;


			comboBoxMap.DataSource = SeaExpTable.ToList();
			comboBoxMap.SelectedIndex = 11;

			comboBoxResult.SelectedIndex = 0;

			var ships = KCDatabase.Instance.Ships;

			if ( ships == null )
				return;

			var list = ships
				.Where( kv => kv.Value.Level > 1 )
				.OrderByDescending( kv => kv.Value.ExpTotal )
				.Select( kv => new ShipItem { Ship = kv.Value } ).ToList();

			comboBoxStartShip.DataSource = list;
			if ( list.Count > 0 )
			{
				radioStartShip.Enabled = true;
			}
		}

		bool flag = false;

		private void numericStartLevel_ValueChanged( object sender, EventArgs e )
		{
			if ( flag )
				flag = false;
			else
				textStartExp.Text = ExpTable[(int)numericStartLevel.Value].ToString();
		}

		private void textStartExp_TextChanged( object sender, EventArgs e )
		{
			int exp;
			if ( int.TryParse( textStartExp.Text, out exp ) )
			{
				for ( int i = ExpTable.Length - 1; i >= 0; i-- )
				{
					if ( ExpTable[i] <= exp )
					{
						if ( numericStartLevel.Value != i )
						{
							flag = true;
							numericStartLevel.Value = i;
						}
						break;
					}
				}
				StartChanged( sender );
			}
		}

		private void radioStartLevel_CheckedChanged( object sender, EventArgs e )
		{
			if ( radioStartShip.Checked )
			{
				radioNextRemodel.Enabled = true;
				StartChanged( sender );
			}
			else
			{
				if ( radioNextRemodel.Checked )
				{
					radioEndLevel.Checked = true;
				}
				radioNextRemodel.Enabled = false;

				if ( selectedShip == null )
					selectedShip = ( (List<ShipItem>)comboBoxStartShip.DataSource ).First();

				StartChanged( sender );
			}
		}

		private void comboBoxStartShip_SelectedIndexChanged( object sender, EventArgs e )
		{
			selectedShip = (ShipItem)comboBoxStartShip.SelectedItem;
			if ( !radioStartShip.Checked )
				radioStartShip.Checked = true;
			else
			{
				StartChanged( sender );
			}
		}

		void StartChanged( object sender )
		{
			if ( radioStartShip.Checked )
			{
				// 舰娘
				if ( radioEndLevel.Checked )
				{
					int exp;
					if ( int.TryParse( textBoxEndExp.Text, out exp ) && selectedShip.Ship.ExpTotal > exp )
					{
						numericEndLevel.Value = Math.Min( selectedShip.Ship.Level + 1, 150 );
					}
				}
				else
				{
					textBoxEndExp.Text = ( selectedShip.Ship.ExpNextRemodel + selectedShip.Ship.ExpTotal ).ToString();
				}
			}
			else
			{
				// 初始等级
				if ( radioNextRemodel.Checked )
					radioEndLevel.Checked = true;

				int exp1, exp2;
				if ( int.TryParse( textStartExp.Text, out exp1 ) && int.TryParse( textBoxEndExp.Text, out exp2 )
					&& exp1 > exp2 )
				{
					numericEndLevel.Value = Math.Min( numericStartLevel.Value + 1, 150 );
				}
			}

			Calc();
		}

		private void radioEndLevel_CheckedChanged( object sender, EventArgs e )
		{
			if ( radioNextRemodel.Checked )
			{
				textBoxEndExp.Text = ( selectedShip.Ship.ExpTotal + selectedShip.Ship.ExpNextRemodel ).ToString();
			}
		}

		private void numericEndLevel_ValueChanged( object sender, EventArgs e )
		{
			if ( flag )
				flag = false;
			else
				textBoxEndExp.Text = ExpTable[(int)numericEndLevel.Value].ToString();
		}

		private void textBoxEndExp_TextChanged( object sender, EventArgs e )
		{
			int exp;
			if ( int.TryParse( textBoxEndExp.Text, out exp ) )
			{
				for ( int i = ExpTable.Length - 1; i >= 0; i-- )
				{
					if ( ExpTable[i] <= exp )
					{
						if ( numericEndLevel.Value != i )
						{
							flag = true;
							numericEndLevel.Value = i;
						}
						break;
					}
				}
				Calc();
			}
		}

		void Calc()
		{
			if ( comboBoxResult.SelectedIndex < 0
				|| comboBoxMap.SelectedIndex < 0 )
				return;

			int startExp;
			int endExp;

			// start
			if ( radioStartLevel.Checked )
			{
				if ( !int.TryParse( textStartExp.Text, out startExp ) || startExp < 0 )
					return;
			}
			else
			{
				startExp = selectedShip.Ship.ExpTotal;
			}

			// end
			if ( !int.TryParse( textBoxEndExp.Text, out endExp ) || endExp < startExp )
				return;

			// calc
			decimal multiplier = ( checkBoxFlagship.Checked ? 1.5M : 1M )
				* ( checkBoxMvp.Checked ? 2M : 1M )
				* ResultMulties[comboBoxResult.SelectedIndex];

			int exp = (int)Math.Round( ( (KeyValuePair<string, int>)comboBoxMap.SelectedItem ).Value * multiplier );
			int remains = endExp - startExp;
			int count = (int)Math.Ceiling( remains / (decimal)exp );

			textBoxSortieExp.Text = exp.ToString();
			textBoxSortieCount.Text = count.ToString();
		}

		private void comboBoxMap_SelectedIndexChanged( object sender, EventArgs e )
		{
			Calc();
		}

		private void comboBoxResult_SelectedIndexChanged( object sender, EventArgs e )
		{
			Calc();
		}

		private void checkBoxFlagship_CheckedChanged( object sender, EventArgs e )
		{
			Calc();
		}

		private void checkBoxMvp_CheckedChanged( object sender, EventArgs e )
		{
			Calc();
		}
	}

	class ShipItem
	{
		public ShipData Ship { get; set; }
		public override string ToString()
		{
			return Ship.NameWithLevel + "    [" + Ship.ExpTotal + "]";
		}
	}
}
