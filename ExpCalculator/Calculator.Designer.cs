﻿namespace ExpCalculator
{
	partial class Calculator
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.textStartExp = new System.Windows.Forms.TextBox();
			this.comboBoxStartShip = new System.Windows.Forms.ComboBox();
			this.numericStartLevel = new System.Windows.Forms.NumericUpDown();
			this.radioStartShip = new System.Windows.Forms.RadioButton();
			this.radioStartLevel = new System.Windows.Forms.RadioButton();
			this.numericEndLevel = new System.Windows.Forms.NumericUpDown();
			this.textBoxEndExp = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.comboBoxMap = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this.comboBoxResult = new System.Windows.Forms.ComboBox();
			this.checkBoxFlagship = new System.Windows.Forms.CheckBox();
			this.checkBoxMvp = new System.Windows.Forms.CheckBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.textBoxSortieCount = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.textBoxSortieExp = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.radioNextRemodel = new System.Windows.Forms.RadioButton();
			this.radioEndLevel = new System.Windows.Forms.RadioButton();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericStartLevel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericEndLevel)).BeginInit();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.textStartExp);
			this.groupBox1.Controls.Add(this.comboBoxStartShip);
			this.groupBox1.Controls.Add(this.numericStartLevel);
			this.groupBox1.Controls.Add(this.radioStartShip);
			this.groupBox1.Controls.Add(this.radioStartLevel);
			this.groupBox1.Location = new System.Drawing.Point(14, 14);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(338, 92);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "初始经验：";
			// 
			// textStartExp
			// 
			this.textStartExp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.textStartExp.Location = new System.Drawing.Point(211, 21);
			this.textStartExp.Name = "textStartExp";
			this.textStartExp.Size = new System.Drawing.Size(119, 23);
			this.textStartExp.TabIndex = 3;
			this.textStartExp.Text = "43500";
			this.textStartExp.TextChanged += new System.EventHandler(this.textStartExp_TextChanged);
			// 
			// comboBoxStartShip
			// 
			this.comboBoxStartShip.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.comboBoxStartShip.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxStartShip.FormattingEnabled = true;
			this.comboBoxStartShip.Location = new System.Drawing.Point(113, 52);
			this.comboBoxStartShip.Name = "comboBoxStartShip";
			this.comboBoxStartShip.Size = new System.Drawing.Size(217, 23);
			this.comboBoxStartShip.TabIndex = 3;
			this.comboBoxStartShip.SelectedIndexChanged += new System.EventHandler(this.comboBoxStartShip_SelectedIndexChanged);
			// 
			// numericStartLevel
			// 
			this.numericStartLevel.Location = new System.Drawing.Point(113, 21);
			this.numericStartLevel.Maximum = new decimal(new int[] {
            150,
            0,
            0,
            0});
			this.numericStartLevel.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numericStartLevel.Name = "numericStartLevel";
			this.numericStartLevel.Size = new System.Drawing.Size(92, 23);
			this.numericStartLevel.TabIndex = 2;
			this.numericStartLevel.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
			this.numericStartLevel.ValueChanged += new System.EventHandler(this.numericStartLevel_ValueChanged);
			// 
			// radioStartShip
			// 
			this.radioStartShip.AutoSize = true;
			this.radioStartShip.Enabled = false;
			this.radioStartShip.Location = new System.Drawing.Point(7, 53);
			this.radioStartShip.Name = "radioStartShip";
			this.radioStartShip.Size = new System.Drawing.Size(85, 19);
			this.radioStartShip.TabIndex = 1;
			this.radioStartShip.Text = "选择舰娘：";
			this.radioStartShip.UseVisualStyleBackColor = true;
			// 
			// radioStartLevel
			// 
			this.radioStartLevel.AutoSize = true;
			this.radioStartLevel.Checked = true;
			this.radioStartLevel.Location = new System.Drawing.Point(7, 22);
			this.radioStartLevel.Name = "radioStartLevel";
			this.radioStartLevel.Size = new System.Drawing.Size(61, 19);
			this.radioStartLevel.TabIndex = 0;
			this.radioStartLevel.TabStop = true;
			this.radioStartLevel.Text = "等级：";
			this.radioStartLevel.UseVisualStyleBackColor = true;
			this.radioStartLevel.CheckedChanged += new System.EventHandler(this.radioStartLevel_CheckedChanged);
			// 
			// numericEndLevel
			// 
			this.numericEndLevel.Location = new System.Drawing.Point(113, 33);
			this.numericEndLevel.Maximum = new decimal(new int[] {
            150,
            0,
            0,
            0});
			this.numericEndLevel.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numericEndLevel.Name = "numericEndLevel";
			this.numericEndLevel.Size = new System.Drawing.Size(92, 23);
			this.numericEndLevel.TabIndex = 2;
			this.numericEndLevel.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
			this.numericEndLevel.ValueChanged += new System.EventHandler(this.numericEndLevel_ValueChanged);
			// 
			// textBoxEndExp
			// 
			this.textBoxEndExp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxEndExp.Location = new System.Drawing.Point(211, 33);
			this.textBoxEndExp.Name = "textBoxEndExp";
			this.textBoxEndExp.Size = new System.Drawing.Size(119, 23);
			this.textBoxEndExp.TabIndex = 3;
			this.textBoxEndExp.Text = "383000";
			this.textBoxEndExp.TextChanged += new System.EventHandler(this.textBoxEndExp_TextChanged);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(6, 19);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(43, 15);
			this.label2.TabIndex = 4;
			this.label2.Text = "地图：";
			// 
			// comboBoxMap
			// 
			this.comboBoxMap.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxMap.FormattingEnabled = true;
			this.comboBoxMap.Location = new System.Drawing.Point(86, 16);
			this.comboBoxMap.Name = "comboBoxMap";
			this.comboBoxMap.Size = new System.Drawing.Size(121, 23);
			this.comboBoxMap.TabIndex = 5;
			this.comboBoxMap.SelectedIndexChanged += new System.EventHandler(this.comboBoxMap_SelectedIndexChanged);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(213, 19);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(43, 15);
			this.label3.TabIndex = 6;
			this.label3.Text = "评价：";
			// 
			// comboBoxResult
			// 
			this.comboBoxResult.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxResult.FormattingEnabled = true;
			this.comboBoxResult.Items.AddRange(new object[] {
            "S",
            "A",
            "B",
            "C",
            "D",
            "E"});
			this.comboBoxResult.Location = new System.Drawing.Point(262, 16);
			this.comboBoxResult.Name = "comboBoxResult";
			this.comboBoxResult.Size = new System.Drawing.Size(70, 23);
			this.comboBoxResult.TabIndex = 7;
			this.comboBoxResult.SelectedIndexChanged += new System.EventHandler(this.comboBoxResult_SelectedIndexChanged);
			// 
			// checkBoxFlagship
			// 
			this.checkBoxFlagship.AutoSize = true;
			this.checkBoxFlagship.Checked = true;
			this.checkBoxFlagship.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxFlagship.Location = new System.Drawing.Point(86, 45);
			this.checkBoxFlagship.Name = "checkBoxFlagship";
			this.checkBoxFlagship.Size = new System.Drawing.Size(50, 19);
			this.checkBoxFlagship.TabIndex = 8;
			this.checkBoxFlagship.Text = "旗舰";
			this.checkBoxFlagship.UseVisualStyleBackColor = true;
			this.checkBoxFlagship.CheckedChanged += new System.EventHandler(this.checkBoxFlagship_CheckedChanged);
			// 
			// checkBoxMvp
			// 
			this.checkBoxMvp.AutoSize = true;
			this.checkBoxMvp.Checked = true;
			this.checkBoxMvp.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxMvp.Location = new System.Drawing.Point(154, 45);
			this.checkBoxMvp.Name = "checkBoxMvp";
			this.checkBoxMvp.Size = new System.Drawing.Size(53, 19);
			this.checkBoxMvp.TabIndex = 9;
			this.checkBoxMvp.Text = "MVP";
			this.checkBoxMvp.UseVisualStyleBackColor = true;
			this.checkBoxMvp.CheckedChanged += new System.EventHandler(this.checkBoxMvp_CheckedChanged);
			// 
			// groupBox2
			// 
			this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox2.Controls.Add(this.textBoxSortieCount);
			this.groupBox2.Controls.Add(this.checkBoxMvp);
			this.groupBox2.Controls.Add(this.label5);
			this.groupBox2.Controls.Add(this.checkBoxFlagship);
			this.groupBox2.Controls.Add(this.textBoxSortieExp);
			this.groupBox2.Controls.Add(this.comboBoxResult);
			this.groupBox2.Controls.Add(this.label4);
			this.groupBox2.Controls.Add(this.label3);
			this.groupBox2.Controls.Add(this.label2);
			this.groupBox2.Controls.Add(this.comboBoxMap);
			this.groupBox2.Location = new System.Drawing.Point(14, 199);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(338, 110);
			this.groupBox2.TabIndex = 10;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "结果：";
			// 
			// textBoxSortieCount
			// 
			this.textBoxSortieCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxSortieCount.BackColor = System.Drawing.Color.White;
			this.textBoxSortieCount.Location = new System.Drawing.Point(256, 71);
			this.textBoxSortieCount.Name = "textBoxSortieCount";
			this.textBoxSortieCount.ReadOnly = true;
			this.textBoxSortieCount.Size = new System.Drawing.Size(74, 23);
			this.textBoxSortieCount.TabIndex = 3;
			// 
			// label5
			// 
			this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(237, 76);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(13, 15);
			this.label5.TabIndex = 2;
			this.label5.Text = "x";
			// 
			// textBoxSortieExp
			// 
			this.textBoxSortieExp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxSortieExp.BackColor = System.Drawing.Color.White;
			this.textBoxSortieExp.Location = new System.Drawing.Point(55, 71);
			this.textBoxSortieExp.Name = "textBoxSortieExp";
			this.textBoxSortieExp.ReadOnly = true;
			this.textBoxSortieExp.Size = new System.Drawing.Size(176, 23);
			this.textBoxSortieExp.TabIndex = 1;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(6, 75);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(43, 15);
			this.label4.TabIndex = 0;
			this.label4.Text = "经验：";
			// 
			// groupBox3
			// 
			this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox3.Controls.Add(this.radioNextRemodel);
			this.groupBox3.Controls.Add(this.radioEndLevel);
			this.groupBox3.Controls.Add(this.numericEndLevel);
			this.groupBox3.Controls.Add(this.textBoxEndExp);
			this.groupBox3.Location = new System.Drawing.Point(14, 112);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(338, 81);
			this.groupBox3.TabIndex = 11;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "目标：";
			// 
			// radioNextRemodel
			// 
			this.radioNextRemodel.AutoSize = true;
			this.radioNextRemodel.Enabled = false;
			this.radioNextRemodel.Location = new System.Drawing.Point(7, 47);
			this.radioNextRemodel.Name = "radioNextRemodel";
			this.radioNextRemodel.Size = new System.Drawing.Size(85, 19);
			this.radioNextRemodel.TabIndex = 4;
			this.radioNextRemodel.Text = "下次改造：";
			this.radioNextRemodel.UseVisualStyleBackColor = true;
			// 
			// radioEndLevel
			// 
			this.radioEndLevel.AutoSize = true;
			this.radioEndLevel.Checked = true;
			this.radioEndLevel.Location = new System.Drawing.Point(7, 22);
			this.radioEndLevel.Name = "radioEndLevel";
			this.radioEndLevel.Size = new System.Drawing.Size(85, 19);
			this.radioEndLevel.TabIndex = 0;
			this.radioEndLevel.TabStop = true;
			this.radioEndLevel.Text = "目标等级：";
			this.radioEndLevel.UseVisualStyleBackColor = true;
			this.radioEndLevel.CheckedChanged += new System.EventHandler(this.radioEndLevel_CheckedChanged);
			// 
			// Calculator
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(366, 321);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.MinimumSize = new System.Drawing.Size(382, 360);
			this.Name = "Calculator";
			this.Text = "经验计算";
			this.Load += new System.EventHandler(this.Calculator_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericStartLevel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericEndLevel)).EndInit();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.ComboBox comboBoxStartShip;
		private System.Windows.Forms.NumericUpDown numericStartLevel;
		private System.Windows.Forms.RadioButton radioStartShip;
		private System.Windows.Forms.RadioButton radioStartLevel;
		private System.Windows.Forms.NumericUpDown numericEndLevel;
		private System.Windows.Forms.TextBox textBoxEndExp;
		private System.Windows.Forms.TextBox textStartExp;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox comboBoxMap;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ComboBox comboBoxResult;
		private System.Windows.Forms.CheckBox checkBoxFlagship;
		private System.Windows.Forms.CheckBox checkBoxMvp;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox textBoxSortieExp;
		private System.Windows.Forms.TextBox textBoxSortieCount;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.RadioButton radioEndLevel;
		private System.Windows.Forms.RadioButton radioNextRemodel;
	}
}