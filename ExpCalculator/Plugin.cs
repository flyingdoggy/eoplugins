﻿using ElectronicObserver.Window.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExpCalculator
{
	public class Plugin : DialogPlugin
	{

		public override string MenuTitle
		{
			get { return "经验计算器(&X)"; }
		}

		public override Form GetToolWindow()
		{
			return new Calculator();
		}
	}
}
